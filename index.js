// console.log("Hello World");

function printInfo(){
	let nickname = prompt("Enter your nickname");
	console.log("Hi, " + nickname);
}

printInfo(); //invoke

				//parameter
function printName(name){
	console.log("My name is " + name);
}

printName("Juana"); //argument
printName("John");
printName("Cena");

// Now we have a reusable function / reusable task but could have different output based on what value to process, with the help off..
// [SECTION] Parameters and Arguments

// Parameter
	// "firstName" is called a parameter
	// A "parameter" acts as named variable/container that exists only inside a function
	// It is used to store information that is provided to a function when it is called/invoke

// Argument
	// "Juana", "John", and "Cena" the information/data provided directly into the function is called "argument."
	// Values passed when invoking a function are called arguments

let sampleVariable = "Inday";

printName(sampleVariable);
// Variables can also be passed as an argument

// -----------------------

function checkDivisibilityBy8(num){
	let	remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibilityBy8 = remainder === 0;
	console.log("Is "+ num + " divisible by 8?");
	console.log(isDivisibilityBy8);
};

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// [SECTION] Function as argument
	// Function parameters can also accept functions as arguments
	// Some complex functions uses other functions to perform more complicated results.

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.")
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	};

	invokeFunction(argumentFunction);
	console.log(argumentFunction);

	// [SECTION] Using Multiple Parameters

		function createFullName(firstName, middleName, lastName){
			console.log("My full name is " + firstName + " " + middleName + " " + lastName);
		}

		createFullName("Jun Edson", "Salilid", "Ilinon");

		// Using variables as an argument
		let firstName = "John";
		let middleName = "Doe";
		let lastName = "Smith";

		createFullName(firstName, middleName, lastName);

		function getDifferenceof8Minus4(numA, numB){
			console.log("Difference: " + (numA - numB));
		};

		getDifferenceof8Minus4(8, 4);
		// getDifferenceof8Minus4(4, 8 ); //This will result to logical error

	// [SECTION] Return Statement

		function returnFullName(firstName, middleName, lastName){
			// return firstName + " " + middleName + " " + lastName;

			// This line of code will not be printed
			console.log("This is printed inside a function");

			// We could also create a variable inside  the function to contain the result and return the variable instead.
			let fullName = firstName + " " + middleName + " " + lastName;
			return fullName;
		};

		let completeName = returnFullName("Jeffrey", "Smith", "Jordan");

		console.log(completeName);

		console.log("I am " + completeName);

		function printPlayerInfo(userName, level, job){
			console.log("Username: " + userName);
			console.log("Level: " + level);
			console.log("Job: " + job);
		}

		let user1 = printPlayerInfo("boxzMapagmahal", "Senior", "Programmer"); // reutrn undefine

		